Open gitbash
Create a project directory (my example is in .../Documents/ACTOR/GitLab/RunnerDemo)
cd to the project directory (i.e.: RunnerDemo)
Create a simple Java program like Hello.java
Create the YAML file, .gitlab-ci.yml -> This is what makes all the magic happen
Create a .gitignore file and add *.class
Create a new project in GitLab to push the repo we are about to create to
Create a repo
  - git init
  - git add .
  - git commit -m "Initial commit!"
 Add the project to GitLab
  - git remote add origin git@gitlab.com:yuseff/gitlab-runner-demo.git
  - git push -u origin master
 Go to GitLab and watch your pipeline run
 